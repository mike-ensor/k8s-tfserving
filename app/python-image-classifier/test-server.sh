#!/bin/bash

# Create temp data

if [ ! -f "./cat.data" ]; then
    base64 -w 0 images/cat-with-big-eyes-square-244x244.png > cat.data
fi
if [ ! -f "./art.data" ]; then
    base64 -w 0 images/ensor-art-square-244x244.png > art.data
fi

# Test with Cat
curl -X POST http://localhost:8080/classify -F image=@cat.data --form "test=mike"

# Test with Artwork
# curl -X POST http://localhost:8080/classify --form "img=@art.data"

