from flask import Flask
from flask import request
from flask import render_template

import requests
import base64
import json

server = Flask(__name__)

IMAGE_DECODE = {}

INFERENCE_HOST="resnet-rest-api.local"
INFERENCE_PORT="80"
INFERENCE_SERVER = 'http://' + INFERENCE_HOST + '/v1/models/resnet_model/labels/stable:predict'

# Structure
# POST /classify [img=base64(img)] <--- returns named classification of input image & displays image
# POST /classify-all [named map of img-name=base64(img)] <--- Same as /classify, just by group
# GET / <--- Returns recent queries

@server.route("/")
def hello():
    return render_template('index.html')

@server.route("/classify", methods=['GET', 'POST'])
def get_classify():
    if request.method == 'POST':
        img = request.files['image']

        encoded_string = base64.b64encode(img.read()).decode()

        predict_request = '{"instances" : [{"b64": "%s"}]}' % encoded_string

        classification = send_query(predict_request)

        return render_template('classify.html', classification=classification)
    else:
        return render_template('classify.html')

def send_query(predict_request):
    # create HTTP POST to endpoint
    # print(predict_request)
    response = requests.post(INFERENCE_SERVER, data=predict_request)
    response.raise_for_status()
    total_time = response.elapsed.total_seconds()
    prediction = response.json()['predictions'][0]

    result = IMAGE_DECODE[str(prediction['classes'])][1]

    return result

if __name__ == "__main__":
    with open('imagenet_class_index.json') as json_file:
        IMAGE_DECODE = json.load(json_file)

    print(IMAGE_DECODE["192"][1])
    server.run(host='0.0.0.0', port=8080)