from __future__ import print_function

import base64
import requests

# The server URL specifies the endpoint of your server running the ResNet
# model with the name "resnet" and using the predict interface.

HOST = '192.168.86.206'# 'localhost'
# HOST = 'localhost'
# PORT = '8080' #
PORT = '8501'
SERVER_URL = 'http://' + HOST + ':' + PORT + '/v1/models/resnet_model/labels/stable:predict'

# The image URL is the location of the image we should send to the server
# IMAGE_URL = 'https://tensorflow.org/images/blogs/serving/cat.jpg'
# IMAGE_URL = 'https://raw.githubusercontent.com/johnny7861532/Convolutional_Neural_Networks/master/dataset/test_set/cats/cat.4001.jpg'
IMAGE_FILE = './images/cat-with-big-eyes-square-244x244.png'
IMAGE_FILE = './images/ensor-art-square-244x244.png'

def main():
  # Download the image
  # TODO: Fix for local file access
  with open(IMAGE_FILE, "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())

  # Needs to be 244x244 pixel
  with open(IMAGE_FILE, "rb") as image_file:
    predict_request = '{"instances" : [{"b64": "%s"}]}' % base64.b64encode(image_file.read()).decode()

  # Send few actual requests and report average latency.
  total_time = 0
  num_requests = 5
  for _ in range(num_requests):
    response = requests.post(SERVER_URL, data=predict_request)
    response.raise_for_status()
    total_time += response.elapsed.total_seconds()
    prediction = response.json()['predictions'][0]

  print('Prediction class: {}, avg latency: {} ms'.format(
      prediction['classes'], (total_time*1000)/num_requests))


if __name__ == '__main__':
  main()