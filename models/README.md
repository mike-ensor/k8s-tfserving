# Purpose

This folder is used to contain any downloaded models for local inspection or used to upload to GCS buckets

>NOTE: All folders are ignored for source repository (via .gitignore)