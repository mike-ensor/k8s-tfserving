# Overview

The purpose of this project is to deploy a [tensorflow-serving](https://github.com/tensorflow/serving) Deployment and Service to a Kubernetes cluster.  The primary goal is to deploy an independent server that references a `saved_model` passed in or attached externally to the server.

### Project Sub goals
1. Show `tensorflow/serving` is decoupled from the model and from the application requesting/querying the inference
1. Deploy an ARM based deployment using: https://github.com/emacski/tensorflow-serving-arm

---

# TOC
0. [tl;dr](#tl;dr)
1. [Create an Embedded Container](#create-an-embedded-container)
    - [Create a local cluster](#1-create-a-local-container)
    - [Push container to gcr.io repository](#2-push-container-to-gcrio-repository)
    - [Create a K8s deployment](#3-create-a-k8s-deployment)
    - [Test embedded deployment](#4-test-embedded-deployment)
2. [Configure Native TensorFlow Container](#configure-native-tensorflow-container)
    - [K8s Deployment using Native container](#1-k8s-deployment-using-native-container)
    - [Test native deployment](#2-test-native-deployment)
3. [Deploy to Raspberry Pi cluster](#deploy-to-raspberry-pi-cluster)
    - [Test gRPC deployment](#2-test-rbpi-grpc-deployment)
    - [Test REST API deployment](#3-test-rbpi-rest-api-deployment)
4. [Clean up](#clean-up)
5. [Appendix](#appendix)
    - [GCS Model Storage Configuration](#gcs-model-storage)
    - [Common or Known Errors](#common-or-known-errors)

---

# Tutorial Steps

The following are an abridged version of the [serving_kubernetes](https://github.com/tensorflow/serving/blob/master/tensorflow_serving/g3doc/serving_kubernetes.md) tutorial. If there are any errors, refer to the original documentation for more details.

## tl;dr
1. Create a known-working container that responds to API calls (gRPC based)
1. Push container to a gcr.io repo
1. Create a k8s Deployment & Service with known-working container
1. Modify known-deployment manifest to target external model
1. IF using a Raspberry Pi k8s cluster, deploy model with an arm-based tensorflow serving build

>NOTE: This repository contains scripts that run each of the 3 scenarios in sequence located [k8s/build-and-deploy-XXXX.sh](k8s/) for convenience

---

# Create an Embedded Container

The goal is to create a container with a [TensorFlow saved model](https://www.tensorflow.org/guide/saved_model) inside of the container.

## 1. Create a local container

This stage will create a local container with an embedded model, and prove that it is functional. Starting with a known-working model and server is the first step in building a k8s based deployment.

>NOTE: Looks like the resnet model is from 2018, use as-is and in the future, this tutorial will add a more up-to-date version

1. Set WORKDIR environment variable to jump around tutorial
    ```bash
    export WORKDIR=$(pwd)
    ```
1. Download resnet model:
    ```bash
    mkdir ${WORKDIR}/resnet
    curl -s http://download.tensorflow.org/models/official/20181001_resnet/savedmodels/resnet_v2_fp32_savedmodel_NHWC_jpg.tar.gz | tar --strip-components=2 -C ${WORKDIR}/resnet -xvz
    ```
1. Create an instance of `tensorflow/serving` locally
    ```bash
    docker run -d --name serving_base tensorflow/serving
    ```
1. Copy the model into it
    ```bash
    docker cp ${WORKDIR}/resnet serving_base:/models/resnet
    ```
1. Create a new container based on the `serving_base` name (above) with the model embedded
    ```bash
    docker commit --change "ENV MODEL_NAME resnet" serving_base $USER/resnet_serving
    ```
1. Stop and remove the generic serving model
    ```bash
    docker kill serving_base
    docker rm serving_base
    ```
1. Run the new service
    ```bash
    # Run as daemon to have the prompt back
    docker run --rm --publish 8500:8500 -t $USER/resnet_serving &
    ```
1. Query the local service using an existing testing tool/app
    ```bash
    git clone https://github.com/tensorflow/serving
    cd serving
    ```
1. Run the tests from within a docker image (NOTE: This command spawns a new large docker image, ~1.3GB size)
    ```bash
    tools/run_in_docker.sh python tensorflow_serving/example/resnet_client_grpc.py
    ```
1. Output should be long and "json-like" if successful
1. Stop local server
    ```bash
    docker ps
    # Look at the "names" field
    docker container kill <name-of-image>
    ```
## 2. Push container to gcr.io repository

This step will push the locally created container to a GCR repository. Note, this tutorial does not go into detail on how to authenticate for gcr. Please follow the [general tutorial & guidelines](https://cloud.google.com/container-registry/docs/pushing-and-pulling) in the official documentation.

1. Make sure you're authenticated
    ```bash
    gcloud auth configure-docker
    ```
    Look for "gcr.io": "gcloud" (and each of the regions: us, eu, asia, etc)
1. Get your current GCP project
    ```bash
    export PROJECT_ID=$(gcloud config get-value core/project 2> /dev/null)
    echo "Verify this is the intended project: $PROJECT_ID"
    ```
1. Push local container to remote repo using tag and push
    ```bash
    # Set variable (used from here to completion of tutorial)
    export RESNET_IMAGE="gcr.io/${PROJECT_ID}/resnet_serving"
    # Tag with remote coordinates
    docker tag ${USER}/resnet_serving "${RESNET_IMAGE}:v1"
    # Push to GCR
    docker push "${RESNET_IMAGE}:v1"
    # Verify
    gcloud container images list-tags "${RESNET_IMAGE}"
    ```
## 3. Create a K8s deployment

This stage pushes a Deployment and Service manifest with our image based on [the training repository](https://github.com/tensorflow/serving/blob/master/tensorflow_serving/example/resnet_k8s.yaml)

>NOTE: This stage is going to use Kustomize to allow us to substitute the image easily and produce hydrated manifests

1. Verify `kustomize` is installed and working. If `kustomize` is not working,
    ```bash
    # should be 3.8+
    kustomize version
    ```
1. Verify `kubectl`
    ```bash
    # client and server both need to be Major: 1 Minor: 14+
    kubectl version
    ```
1. Generate the manifest file & deploy
    ```bash
    # deploying the Embedded model version
    cd ${WORKDIR}/k8s/overlays/embedded
    kustomize edit set image busybox="${RESNET_IMAGE}:v1"
    # Deploy
    kustomize build ${WORKDIR}/k8s/overlays/embedded | kubectl apply -f -
    # Wait till complete or timeout
    kubectl wait --for=condition=available --timeout=60s deployment resnet-deployment
    ```
    >NOTE: Optionally, running the `k8s/build-and-deploy-embedded.sh` file will perform the same commands

## 4. Test embedded deployment

1. Create a local `port-forward` of the service
    >NOTE: instead of using a `LoadBalancer`, this tutorial uses `NodePort` and `port-forward` to expose the service for the tests (cheaper and doesn't take as long)

    ```bash
    # Create a local-only port
    kubectl port-forward svc/resnet-service-embedded 8500:8500 &
    ```
1. Run the tests again against the now k8s based service exposed locally
    ```bash
    export SERVER_ENDPOINT=localhost
    cd ${WORKDIR}/serving
    ./tools/run_in_docker.sh python \
        tensorflow_serving/example/resnet_client_grpc.py \
        --server=${SERVER_ENDPOINT}:8500
    ```
1. Stop the `port-forward`
    ```bash
    ps -a
    # Look for the kubectl prefixed name,
    killall kubectl-<fill in rest of the command>
    ```

# Configure Native TensorFlow Container

Building on knowledge that our model is functional, the goal is to use the native [`tensorflow/serving` container](https://www.tensorflow.org/tfx/serving/serving_basic). Kubernetes KRM objects will be created to control configuration, deployment and serving. Configuration will be passed in to the Pod via ConfigMaps. Saved models will be stored in a Google Cloud Storage bucket. See [GCS Model Storage](#gcs-model-storage) for instructions on authentication and authorization.

>NOTE: `kustomize` is being used to reduce duplication of Kubernetes resource files.

## 1. K8s Deployment using Native container

1. Create a new GCS bucket
    ```bash
    export PROJECT_ID=$(gcloud config get-value core/project 2> /dev/null)
    export BUCKET_NAME=<some-unique-name>
    gsutil mb ${BUCKET_NAME}
    ```

1. Create a new Google Service Account with key
    ```bash
    export GSA_NAME="gcs-agent"

    gcloud iam service-accounts create ${GSA_NAME} \
        --description="Service account controlling access to saved-model bucket" \
        --display-name="${GSA_NAME}"

    # Create a JSON key
    gcloud iam service-accounts keys create ./service-account-key.json \
        --iam-account ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com

    # Move key to folder ignored by git and used by kustomize
    mv ./service-account-key.json k8s/overlays/native/configuration/secrets

    # IF using Raspberry Pi, copy to the ARM folder
    mv ./service-account-key.json k8s/overlays/arm/configuration/secrets
    ```

1. Deploy the 'native' `tensorflow/serving` container and pass the model configuration to the container
    ```bash
    # deploying the Embedded model version
    cd ${WORKDIR}/k8s/overlays/native
    # Replace BUCKET_NAME with real bucket name
    sed  "s/%%BUCKET_NAME%%/${BUCKET_NAME}/g" configuration/resnet-model-config.conf > configuration/resnet-model-config-hydrated.conf
    # Deploy
    kustomize build ${WORKDIR}/k8s/overlays/native | kubectl apply -f -
    # Wait till complete or timeout
    kubectl wait --for=condition=available --timeout=60s deployment resnet-deployment-native
    ```
    >NOTE: Optionally, running the `k8s/build-and-deploy-native.sh` file will perform the same commands

1. Create a local `port-forward` of the service
    >NOTE: instead of using a `LoadBalancer`, this tutorial uses `NodePort` and `port-forward` to expose the service for the tests (cheaper and doesn't take as long to boot up)

    >INFO: This command uses a "daemon" background process. If you have access to multiple shell sessions (ie, tabs or windows), exclude the "&" and use one shell to `port-forward` and one to run the command

    ```bash
    # Create a local-only port (daemon is optional)
    kubectl port-forward svc/resnet-service-native 8500:8500 &
    ```

## 2. Test Native Deployment

1. Run the tests again against the now k8s based service exposed locally
    >NOTE: This is the EXACT same command used to query the embedded version above
    ```bash
    export SERVER_ENDPOINT=localhost
    cd ${WORKDIR}/serving
    ./tools/run_in_docker.sh python \
        tensorflow_serving/example/resnet_client_grpc.py \
        --server=${SERVER_ENDPOINT}:8500
    ```
1. Stop the `port-forward`
    >NOTE: If the command above is NOT using daemon process (ie, "&" suffix, skip this step)
    ```bash
    ps -a
    # Look for the kubectl prefixed name and replace
    killall kubectl-<fill in rest of the command>
    ```

# Deploy to Raspberry Pi cluster

Deploying to a Raspberry Pi cluster requires the use of an ARM-based `tensorflow/serving` container. [https://github.com/emacski/tensorflow-serving-arm](https://github.com/emacski/tensorflow-serving-arm) provides a container called `emacski/tensorflow-serving` with various ARM-based builds.

>NOTE: This configuration has been tested against a [Turing Pi](https://turingpi.com/) configuration using Hypriot and K3s. In theory, any ARM-based Raspberry Pi cluster should work, but might have different challenges with respect to CNI, CRI, etc.

1. Deploy the ARM deployment

    ```bash
    # deploying the ARM model version
    cd ${WORKDIR}/k8s/overlays/arm
    # Replace BUCKET_NAME with real bucket name
    sed  "s/%%BUCKET_NAME%%/${BUCKET_NAME}/g" configuration/resnet-model-config.conf > configuration/resnet-model-config-hydrated.conf
    # Deploy
    kustomize build ${WORKDIR}/k8s/overlays/arm | kubectl apply -f -
    # Wait till complete or timeout
    kubectl wait --for=condition=available --timeout=60s deployment resnet-deployment-tpi
    ```
    >NOTE: Optionally, running the `k8s/build-and-deploy-turing-pi.sh` file will perform the same commands

## 2. Test RBPi gRPC Deployment

1. Create a local `port-forward` of the service

    >INFO: This command uses a "daemon" background process. If you have access to multiple shell sessions (ie, tabs or windows), exclude the "&" and use one shell to `port-forward` and one to run the command

    ```bash
    # Create a local-only port (daemon is optional)
    kubectl port-forward svc/resnet-service-tpi 8500:8500 &
    ```
1. Run the tests again against the now k8s based service exposed locally
    >NOTE: This is the EXACT same command used to query the embedded version above
    ```bash
    export SERVER_ENDPOINT=localhost
    cd ${WORKDIR}/serving
    ./tools/run_in_docker.sh python \
        tensorflow_serving/example/resnet_client_grpc.py \
        --server=${SERVER_ENDPOINT}:8500
    ```
1. Stop port-forward

    (see any step above to stop port-forward)

## 3. Test RBPi REST API Deployment

1. Test REST API
    The REST APIs run on `8501` instead of gRPC running on `8500`. Similar to the gRPC tests, two supplied Python applications have been provided in `/app`.

    ```bash
    # Create a local-only port of REST api
    kubectl port-forward svc/resnet-service-rest-tpi 8501:8501 &
    ```

    >NOTE: Python 3.7+ has been tested, no other versions have been attempted, use at your own risk. This is not intended to be used as a base for production
    ```bash
    cd ${WORKDIR}/app
    python main.py
    ```
1. Stop the port-forward

    (see any step above to stop port-forward)

# Clean up

1. Easiest way to clean up is to remove the whole Google project. If you want to just un-deploy the services, you can use the script files in the `k8s` folder and add any single letter/character/word after the script to trigger a "delete"

    ```bash
    cd ${WORKDIR}/k8s
    ./build-and-deploy-embedded.sh delete
    ./build-and-deploy-native.sh delete
    ```
1. Don't forget to delete the bucket (`BUCKET_NAME`) containing the model as it's likely a few hundred MB in size. Command-line or web console remove optional.

    ```bash
    # force remove bucket
    gsutil rb -f gs://${BUCKET_NAME}
    ```

# Appendix

## GCS Model Storage

This tutorial is built using Google Cloud to host the saved models and provide authorization. Authentication and Authorization are use Google Cloud IAM and Google Service Accounts. The Pod that hosts the `tensorflow/serving` container needs to have access to a bucket or storage location. The easiest option is to give public access to a bucket, however, this is not an ideal solution.

This solution uses a [Service Account and an associated key](https://cloud.google.com/iam/docs/creating-managing-service-account-keys), where the Google Service Account has `roles/storage.objectViewer` access. The JSON key is `base64` encoded and saved as Kubernetes Secret.  The secret is then exposed as a volume and ultimately a file to the container.  By convention, Google Cloud tools use `GOOGLE_APPLICATION_CREDENTIALS` environment variable to point at a Google Service Account JSON key.

## Common or Known Errors

Below is a list of either known errors or common issues. Some issues are listed in the [GitLab Project Issues](https://gitlab.com/mike-ensor/k8s-tfserving/-/issues) and are being worked on. If you'd like to submit an issue, please do...even better, fork the repo and submit a Merge Request with a fix!

* (TuringPi only) CRI or CNI errors while trying to run the REST API tests against the service. The result will look like "connection refused", even though you can `port-forward` to the service. Often a reboot of the K8s system, or un-deploy, re-deploy everything will fix this. It happened about 10% of the time and did not spend any time trying to investigate.

* OOMKilled status for Pods after a short period of time -- This is an "out of memory" error from Kubernetes. The defaults and limits are listed in the `overlays/**/deployment.yaml` files (for ARM and Native). If this happens, either increase the amount of requested/limited RAM or reduce the number of models deployed or find/build a smaller model (perhaps TensorFlow Lite)

