#!/bin/bash

BASE_DIR=$(pwd)
DEPLOY=true # script-level parameter to skip deploying
CMD="apply"

OVERLAY="embedded"
SUFFIX="embedded"

CMD="apply"

if [ ! -z "$1" ]; then
    echo "Running Delete instead"
    CMD="delete"
fi

cd ${BASE_DIR}/overlays/${OVERLAY}
# Set the image pushed to project-local GCR
kustomize edit set image busybox="${RESNET_IMAGE}:v1"
# Output plan
kustomize build ${BASE_DIR}/overlays/${OVERLAY}
# Deploy
if [ "${DEPLOY}" == "true" ]; then
    # Deploy
    kustomize build ${BASE_DIR}/overlays/${OVERLAY} | kubectl ${CMD} -f -
    # Wait till complete or timeout
    kubectl wait --for=condition=available --timeout=60s deployment resnet-deployment-${SUFFIX}
fi

cd ${BASE_DIR}