#!/bin/bash

BASE_DIR=$(pwd)
DEPLOY=true # script-level parameter to skip deploying
CMD="apply"

OVERLAY="native"
SUFFIX="native"

if [ ! -z "$1" ]; then
    echo "Running Delete instead"
    CMD="delete"
fi

cd ${BASE_DIR}/overlays/${OVERLAY}


if [ -z "${BUCKET_NAME}" ]; then
    echo "ENV: BUCKET_NAME is required. Bucket name is associated with the bucket containing the model"
    exit 1
fi
# Replace bucket name with real bucket
sed  "s/%%BUCKET_NAME%%/${BUCKET_NAME}/g" configuration/resnet-model-config.conf > configuration/resnet-model-config-hydrated.conf

# Output plan
kustomize build ${BASE_DIR}/overlays/${OVERLAY}
# Deploy
if [ "${DEPLOY}" == "true" ]; then
    # Deploy
    kustomize build ${BASE_DIR}/overlays/${OVERLAY} | kubectl ${CMD} -f -
    # Wait till complete or timeout
    kubectl wait --for=condition=available --timeout=60s deployment resnet-deployment-${SUFFIX}
fi

cd ${BASE_DIR}