# Setup

Place your Google Service Account key in this folder and name it:

`service-account-key.json`

Any `*.json` files are ignored by Git per the `.gitignore` file.
